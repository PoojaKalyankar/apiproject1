//Update all checkitems in a checklist to completed status simultaneously

const apiKey = 'fdd0f4f5b736f37f2502dc533da5cf84';
const apiToken = 'ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C';

function checkItems(checklistId) {
    const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${apiKey}&token=${apiToken}`;
    return fetch(url)
        .then(response => response.json())
        .catch(error => {
            console.error(error);
        });
}

function updateCheckToComplete(cardId, checkitemId) {
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=complete&key=${apiKey}&token=${apiToken}`;
    return fetch(url, {
        method: 'PUT'
    })
        .then(response => response.json())
        .catch(error => {
            console.error(`${checkitemId}`, error);
        });
}

function updateAllCheckitemsToCompleted(checklistId, cardId) {
    return checkItems(checklistId)
        .then(checkitems => {
            const updatePromises = checkitems.map(checkitem =>
                updateCheckToComplete(cardId, checkitem.id)
            );
            return Promise.all(updatePromises);
        })
        .then(() => {
            console.log('check items are updated');
        })
        .catch(error => {
            console.error(error);
        });
}

const checklistId = '6654fdd931d09bb5df64bc4d';
const cardId = '6654fda97170b88d5d523e65';
updateAllCheckitemsToCompleted(checklistId, cardId);


