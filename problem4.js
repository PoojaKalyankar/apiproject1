//Create a function getCards which takes a listId as 
//argument and returns a promise which resolves with cards data

function getCards(listId) {
    return new Promise(function (resolve, reject) {
        const url = `https://api.trello.com/1/lists/${listId}/cards?key=fdd0f4f5b736f37f2502dc533da5cf84&token=ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C`
        fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                resolve(response.json())
            })
    })

}

getCards('66544fe5975dfbde64863b29')
    .then(response => {
        console.log(response)
    })
    .catch(error => {
        console.error(error)
    })

module.exports = getCards

