//Create a new board, create 3 lists simultaneously, 
//and a card in each list simultaneously

const createBoard = require('./problem2')

function creationListAndCard(board) {
    return new Promise((resolve, reject) => {
        createBoard(board)
            .then(data => {
                return Promise.all([createLists('list1', data.id)
                    , createLists('list2', data.id), createLists('list3', data.id)])
            })
            .then(data => {
                return Promise.all(data.map(list => createCards(list.id)))
            })
            .then(data => {

                resolve(data)
            })
    })
}

creationListAndCard('thirdBoard')
    .then(data => {
        console.log(data)
    })
    .catch(error => {
        console.error(error)
    })

function createLists(name, id) {
    return new Promise((resolve, reject) => {
        let url = `https://api.trello.com/1/lists?name=${name}&idBoard=${id}&key=fdd0f4f5b736f37f2502dc533da5cf84&token=ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C`
        fetch(url, {
            method: 'POST'
        })
            .then(response => {
                return response.json()

            })
            .then(data => {
                resolve(data)
            })
            .catch(error => {
                console.error(error)
            })
    })

}

function createCards(id) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/cards?idList=${id}&key=fdd0f4f5b736f37f2502dc533da5cf84&token=ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                //console.log(response)
                return response.json();
            })
            .then(data => {
                console.log(data)
                resolve(data)
            })
            .catch(error => {
                console.error(error)
            })
    })
}


module.exports = creationListAndCard




