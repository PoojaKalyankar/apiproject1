//Delete all the lists created in Step 6 sequentially 
//i.e. List 1 should be deleted -> then List 2 should be deleted etc

const createBoard = require("./problem6");
const key = 'fdd0f4f5b736f37f2502dc533da5cf84';
const token = 'ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C';

function creating() {
    return new Promise((resolve, reject) => {
        createBoard().then((data) => {
            for (let list of data) {
                deleteList(list.idList)
            }
        }).then(resolve('deleted'))
    })
}

function deleteList(listID) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists/${listID}/closed?value=true&key=${key}&token=${token}`, {
            method: "PUT",
        })
            .then(response => response.json())
            .then((deletedCard) => {
                resolve(deletedCard);
            }).catch((error) => {
                reject(error);
            });
    });
}

creating().then((result) => { console.log(result) })
