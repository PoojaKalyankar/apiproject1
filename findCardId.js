//finding card id
const API_KEY = 'fdd0f4f5b736f37f2502dc533da5cf84';
const TOKEN = 'ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C';
const CARD_ID = '6654fda97170b88d5d523e65'

function getChecklists(cardId) {
    return fetch(`https://api.trello.com/1/cards/${cardId}/checklists?key=${API_KEY}&token=${TOKEN}`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            return data;
        })
        .catch(err => console.error(err));
}

getChecklists(CARD_ID)
    .then(checklists => {
        checklists.forEach(checklist => {
            console.log(`id: ${checklist.id}, name: ${checklist.name}`);
        });
    });