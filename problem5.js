//Create a function getAllCards which takes a boardId as argument and
// which uses getCards function to fetch cards of all the lists. 
//Do note that the cards should be fetched simultaneously from all the lists.
const getCards = require('./problem4')
const getLists = require('./problem3')

function getAllCards(boardId) {
    return new Promise((resolve, reject) => {
        getLists(boardId)
            .then(list => {
                let array = list.map(listObj => getCards(listObj.id))
                Promise.all(array)
                    .then(data => {
                        resolve(data)
                    })

            })
    })
}

getAllCards('66544fe5975dfbde64863b22')
    .then(data => {
        console.log(data)
    })
    .catch(error => {
        console.error(error)
    })
//console.log(getCards)
