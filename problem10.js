//Update all checkitems in a checklist to incomplete status 
//sequentially i.e. Item 1 should be updated -> then wait for
// 1 second -> then Item 2 should be updated etc.

const apiKey = 'fdd0f4f5b736f37f2502dc533da5cf84';
const apiToken = 'ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C';

function fetchCheckitems(checklistId) {
    const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${apiKey}&token=${apiToken}`;
    return fetch(url)
        .then(response => response.json())
        .catch(error => {
            console.error(error);
        });
}

function updateToIncomplete(cardId, checkitemId) {
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=incomplete&key=${apiKey}&token=${apiToken}`;
    return fetch(url, {
        method: 'PUT'
    })
    .then(response => response.json())
    .catch(error => {
        console.error(error);
        throw error;
    });
}

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function updateToIncompleteSeq(checklistId, cardId) {
    return fetchCheckitems(checklistId)
        .then(checkitems => {
            const updatePromises = checkitems.map(checkitem => {
                return () => updateToIncomplete(cardId, checkitem.id)
                    .then(() => delay(1000)); 
            });
            return updatePromises.reduce((chain, update) => {
                return chain.then(update);
            }, Promise.resolve());
        })
        .then(() => {
            console.log('all checkitems are marked as incomplete');
        })
        .catch(error => {
            console.error(error);
        });
}

const checklistId = '6654fdd931d09bb5df64bc4d';
const cardId = '6654fda97170b88d5d523e65';
updateToIncompleteSeq(checklistId, cardId);
