//Create a function createBoard which takes the boardName 
//as argument and returns a promise 
//which resolves with newly created board data

function createBoard(newBoardName) {
    return new Promise((resolve, reject) => {
        let url = `https://api.trello.com/1/boards/?name=${newBoardName}&key=fdd0f4f5b736f37f2502dc533da5cf84&token=ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C`
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                resolve(response.json())
            })
    })

}

createBoard('secondBoard')
    .then((data) => {
        console.log(data)
    })
    .catch(error => {
        console.error(error)
    })


module.exports = createBoard