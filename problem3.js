//Create a function getLists which takes a boardId 
//as argument and returns a promise which 
//resolved with lists data

function getLists(boardId) {
    return new Promise((resolve, reject) => {
        const url = `https://api.trello.com/1/boards/${boardId}/lists?key=fdd0f4f5b736f37f2502dc533da5cf84&token=ATTAd8bf9f41d59d40f7b21c2e7544c5daca01392727d3fd4d0a865f6f4194d655a6A082E72C`;

        fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                resolve(response.json())
            })
    })
}

getLists("66544fe5975dfbde64863b22")
    .then(data => {
        console.log(data)
    })
    .catch(error => {
        console.error(error)
    })

module.exports = getLists;


